package com.crud.h2.dto;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;

@Entity
@Table(name="fabricantes")
public class Fabricante {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@NotNull  
	private Long id;
	@NotNull  
	@Column(name = "nombre", length=100)  
	private String nombre;
	
	@OneToMany
    @JoinColumn(name="id")	
    private List<Articulo> artiuclo;	
	
	//Constructores
	public Fabricante() {
		
	}

	public Fabricante(Long id, String nombre, List<Articulo> artiuclo) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.artiuclo = artiuclo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@JsonIgnore
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "Articulo")
	public List<Articulo> getArtiuclo() {
		return artiuclo;
	}

	public void setArtiuclo(List<Articulo> artiuclo) {
		this.artiuclo = artiuclo;
	}
	
	
	
}
