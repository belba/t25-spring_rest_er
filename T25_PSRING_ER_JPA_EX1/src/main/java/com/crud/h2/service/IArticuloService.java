package com.crud.h2.service;

import java.util.List;
import org.springframework.stereotype.Service;
import com.crud.h2.dto.Articulo;

public interface IArticuloService {

	//Metodos del cRUD
	public List<Articulo> getAllArticulos();
	
	public Articulo saveArticulo(Articulo articulo);
	
	public Articulo getArticulo(Long id);
	
	public Articulo updateArticulo(Articulo articulo);
	
	public void deleteArticulo(Long id);
	
}
