package com.crud.h2.service;

import java.util.List;

import com.crud.h2.dto.Fabricante;

public interface IFabricanteService {
	
	//Metodo del CRUD
	public List<Fabricante> getAllFabricantes();
	
	public Fabricante getFabricante(Long id);
	
	public Fabricante saveFabricante(Fabricante fabricante);
	
	public Fabricante updateFabricante(Fabricante fabricante);
	
	public void deleteEmpleado(Long id);
}
