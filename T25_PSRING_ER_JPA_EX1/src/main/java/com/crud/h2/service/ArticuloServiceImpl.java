package com.crud.h2.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crud.h2.dao.IArticuloDao;
import com.crud.h2.dto.Articulo;

@Service
public class ArticuloServiceImpl implements IArticuloService{

	@Autowired
	IArticuloDao iArticuloDao;
	

	@Override
	public Articulo getArticulo(Long id) {
		// TODO Auto-generated method stub
		return iArticuloDao.findById(id).get();
	}

	@Override
	public Articulo updateArticulo(Articulo articulo) {
		// TODO Auto-generated method stub
		return iArticuloDao.save(articulo);
	}

	@Override
	public void deleteArticulo(Long id) {
		// TODO Auto-generated method stub
		iArticuloDao.deleteById(id);
	}

	@Override
	public Articulo saveArticulo(Articulo articulo) {
		// TODO Auto-generated method stub
		return iArticuloDao.save(articulo);
	}

	@Override
	public List<Articulo> getAllArticulos() {
		// TODO Auto-generated method stub
		return iArticuloDao.findAll();
	}

}
