package com.crud.h2.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crud.h2.dao.IFabricanteDao;
import com.crud.h2.dto.Fabricante;

@Service
public class FabricanteServiceImpl implements IFabricanteService{

	@Autowired
	IFabricanteDao iFabricanteDao;

	@Override
	public Fabricante updateFabricante(Fabricante fabricante) {
		// TODO Auto-generated method stub
		return iFabricanteDao.save(fabricante);
	}

	@Override
	public void deleteEmpleado(Long id) {
		// TODO Auto-generated method stub
		iFabricanteDao.deleteById(id);
	}

	@Override
	public List<Fabricante> getAllFabricantes() {
		// TODO Auto-generated method stub
		return iFabricanteDao.findAll();
	}

	@Override
	public Fabricante getFabricante(Long id) {
		// TODO Auto-generated method stub
		return iFabricanteDao.findById(id).get();
	}

	@Override
	public Fabricante saveFabricante(Fabricante fabricante) {
		// TODO Auto-generated method stub
		return iFabricanteDao.save(fabricante);
	}

}
