package com.crud.h2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crud.h2.dto.Articulo;
import com.crud.h2.service.ArticuloServiceImpl;

@RestController
@RequestMapping("/api")
public class ArticuloController {

	@Autowired
	ArticuloServiceImpl articuloServiceImpl;
	
	@GetMapping("/articulos")
	public List<Articulo> getAllArticulos(){
		return articuloServiceImpl.getAllArticulos();
	}

	@GetMapping("/articulos/{id}")
	public Articulo getArticulo(@PathVariable(name="id") Long id) {
		Articulo art = new Articulo();
		art = articuloServiceImpl.getArticulo(id);
		return art;
	}
	
	@PostMapping("/arituclos")
	public Articulo saveArticulo(@RequestBody Articulo articulo) {
		return articuloServiceImpl.saveArticulo(articulo);
	}	
	
	@PutMapping("/articulos/{id}")
	public Articulo updateArticulo(@PathVariable(name="id") Long id, @RequestBody Articulo articulo){
		Articulo art_select = new Articulo();
		Articulo art_updated = new Articulo();
		
		art_select = articuloServiceImpl.getArticulo(id);
		
		art_select.setNombre(articulo.getNombre());
		art_select.setPrecio(articulo.getPrecio());
		art_select.setFabricante(articulo.getFabricante());
	
		art_updated = articuloServiceImpl.updateArticulo(art_select);
		
		return art_updated;
	}
	
	@DeleteMapping("/articulos/{id}")
	public void eliminarArticulo(@PathVariable(name="id") Long id) {
		articuloServiceImpl.deleteArticulo(id);
	}

}
