package com.crud.h2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crud.h2.dto.Fabricante;
import com.crud.h2.service.FabricanteServiceImpl;

@RestController
@RequestMapping("/api")
public class FabricanteController {

	@Autowired
	FabricanteServiceImpl fabricanteServiceImpl;
	
	@GetMapping("/fabricantes")
	public List<Fabricante> getAllFabricante(){
		return fabricanteServiceImpl.getAllFabricantes();
	}
	
	@GetMapping("/fabricantes/{id}")
	public Fabricante getFabricante(@PathVariable(name="id") Long id) {
		Fabricante fabricante = new Fabricante();
		fabricante = fabricanteServiceImpl.getFabricante(id);
		return fabricante;
	}
	
	@PostMapping("/fabricantes")
	public Fabricante saveFabricante(@RequestBody Fabricante fabricante) {
		return fabricanteServiceImpl.saveFabricante(fabricante);
	}	
	
	@PutMapping("/fabricantes/{id}")
	public Fabricante updateFabricante(@PathVariable(name="id") Long id, @RequestBody Fabricante fabricante) {
		Fabricante fab_selected = new Fabricante();
		Fabricante fab_updated = new Fabricante();
		
		fab_selected = fabricanteServiceImpl.getFabricante(id);
	
		fab_selected.setId(fabricante.getId());
		fab_selected.setNombre(fabricante.getNombre());
		
		fab_updated = fabricanteServiceImpl.updateFabricante(fab_selected);
		
		return fab_updated;
	}
	
	@DeleteMapping("/fabricantes/{id}")
	public void deleteFabricante(@PathVariable(name="id") Long id) {
		fabricanteServiceImpl.deleteEmpleado(id);
	}
	
	
}
