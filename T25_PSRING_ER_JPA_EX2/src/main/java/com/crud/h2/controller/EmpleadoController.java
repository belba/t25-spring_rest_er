package com.crud.h2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crud.h2.dao.IEmpleadoDao;
import com.crud.h2.dto.Empleado;
import com.crud.h2.service.EmpleadoServiceImpl;
import com.crud.h2.service.IEmpleadoService;

@RestController
@RequestMapping("/api")
public class EmpleadoController {

	@Autowired
	private IEmpleadoService empleadoService;

	@GetMapping("/empleados")
	public List<Empleado> getAllEmpleados() {
		return empleadoService.getAllEmpleados();
	}

	@GetMapping("/empleados/{id")
	public Empleado getEmpleado(@PathVariable(name = "id") String id) {
		Empleado emp = new Empleado();
		emp = empleadoService.getEmpleado(id);
		return emp;
	}

	@PostMapping("/empleados")
	public Empleado saveEmpleado(@RequestBody Empleado empleado) {
		return empleadoService.saveEmpleado(empleado);
	}

	@PutMapping("/empleados/{id}")
	public Empleado updateEmpleado(@PathVariable(name = "id") String id, @RequestBody Empleado empleado) {
		Empleado emp_select = new Empleado();
		Empleado emp_updated = new Empleado();

		emp_select = empleadoService.getEmpleado(id);

		emp_select.setDni(empleado.getDni());
		emp_select.setNombre(empleado.getNombre());
		emp_select.setApellido(empleado.getApellido());
		emp_select.setDepartamento(empleado.getDepartamento());

		emp_updated = empleadoService.updateEmpleado(emp_select);

		return emp_updated;
	}

	@DeleteMapping("/empleados/{id}")
	public void deleteEmpleado(@PathVariable(name = "id") String id) {
		empleadoService.deleteEmpleado(id);
	}

}
