package com.crud.h2.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.crud.h2.dto.Departamento;
import com.crud.h2.service.IDepartamentoService;

@RestController
@RequestMapping("/api")
public class DepartamentoController {

	@Autowired
	private IDepartamentoService departamentoService;
	
	@GetMapping("/departamentos")
	public List<Departamento> getAllDepartamentos(){
		return departamentoService.getAllDepartametos();
	}
	
	@GetMapping("/departamento/{id}")
	public Departamento getDepartamento(@PathVariable(name="id")Long id) {
		Departamento dep = new Departamento();
		dep = departamentoService.getDepartamento(id);
		return dep;
	}

	@PostMapping("/departamento/{id}")
	public Departamento saveDepartamento(@RequestBody Departamento departamento) {
		return departamentoService.saveDepartamento(departamento);
	}
	
	@PutMapping("/departamento/{id}")
	public Departamento updateDepartamento(@PathVariable(name="id") Long id, @RequestBody Departamento departamento){
		Departamento dep_select = new Departamento();
		Departamento dep_updated = new Departamento();
		
		dep_select = departamentoService.getDepartamento(id);
		
		dep_select.setId(departamento.getId());
		dep_select.setNombre(departamento.getNombre());
		dep_select.setPresupuesto(departamento.getPresupuesto());
		dep_select.setEmpleados(departamento.getEmpleados());

		dep_updated = departamentoService.updateDepartamento(dep_select);
		
		return dep_updated;
		
	}	
	
	@DeleteMapping("/departamento/{id}")
	public void deleteDepartamento(@PathVariable(name="id") Long id) {
		departamentoService.DeleteDepartamento(id);
	}
}
