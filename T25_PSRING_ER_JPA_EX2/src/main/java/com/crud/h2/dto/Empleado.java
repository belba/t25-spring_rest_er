package com.crud.h2.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.sun.istack.NotNull;

@Entity
@Table(name="empleados")
public class Empleado {

	@Id
	@Column(name = "dni", length=8)
	@NotNull
	private String dni; 
	
	@Column(name = "nombre", length=100)
	private String nombre; 
	
	@Column(name = "apellido", length=255)
	private String apellido; 
	
	@ManyToOne
    @JoinColumn(name="dep_id")
    private Departamento departamento;

	public Empleado() {
		super();
	}

	public Empleado(String dni, String nombre, String apellido, Departamento departamento) {
		super();
		this.dni = dni;
		this.nombre = nombre;
		this.apellido = apellido;
		this.departamento = departamento;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}
	
}
