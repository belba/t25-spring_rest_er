package com.crud.h2.service;

import java.util.List;

import com.crud.h2.dto.Departamento;

public interface IDepartamentoService {

	//Metodos del CRUD
	public List<Departamento> getAllDepartametos();

	public Departamento saveDepartamento(Departamento departamento);
	
	public Departamento getDepartamento(Long id);
	
	public Departamento updateDepartamento(Departamento departamento);
	
	public void DeleteDepartamento(Long id);
	

}
