package com.crud.h2.service;

import java.util.List;

import com.crud.h2.dto.Empleado;

public interface IEmpleadoService {
	
	//Metodos del CRUD
	public List<Empleado> getAllEmpleados();
	
	public Empleado saveEmpleado(Empleado empleado);
	
	public Empleado getEmpleado(String id);
	
	public Empleado updateEmpleado(Empleado empleado);
	
	public void deleteEmpleado(String id);

}
