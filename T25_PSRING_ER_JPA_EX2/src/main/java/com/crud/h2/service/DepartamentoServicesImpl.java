package com.crud.h2.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crud.h2.dao.IDepartamentoDao;
import com.crud.h2.dto.Departamento;

@Service
public class DepartamentoServicesImpl implements IDepartamentoService{

	@Autowired
	IDepartamentoDao iDepartamentoDao;
	
	@Override
	public List<Departamento> getAllDepartametos() {
		// TODO Auto-generated method stub
		return iDepartamentoDao.findAll();
	}

	@Override
	public Departamento saveDepartamento(Departamento departamento) {
		// TODO Auto-generated method stub
		return iDepartamentoDao.save(departamento);
	}

	@Override
	public Departamento getDepartamento(Long id) {
		// TODO Auto-generated method stub
		return iDepartamentoDao.findById(id).get();
	}

	@Override
	public Departamento updateDepartamento(Departamento departamento) {
		// TODO Auto-generated method stub
		return iDepartamentoDao.save(departamento);
	}

	@Override
	public void DeleteDepartamento(Long id) {
		// TODO Auto-generated method stub
		iDepartamentoDao.deleteById(id);
	}

}
