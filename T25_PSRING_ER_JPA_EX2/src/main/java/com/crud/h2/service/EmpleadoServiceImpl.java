package com.crud.h2.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.crud.h2.dao.IEmpleadoDao;
import com.crud.h2.dto.Empleado;

@Service
public class EmpleadoServiceImpl implements IEmpleadoService{

	@Autowired
	IEmpleadoDao iEmpleadoDao;

	@Override
	public List<Empleado> getAllEmpleados() {
		// TODO Auto-generated method stub
		return iEmpleadoDao.findAll();
	}

	@Override
	public Empleado saveEmpleado(Empleado empleado) {
		// TODO Auto-generated method stub
		return iEmpleadoDao.save(empleado);
	}

	@Override
	public Empleado getEmpleado(String id) {
		// TODO Auto-generated method stub
		return iEmpleadoDao.findById(id).get();
	}

	@Override
	public Empleado updateEmpleado(Empleado empleado) {
		// TODO Auto-generated method stub
		return iEmpleadoDao.save(empleado);
	}

	@Override
	public void deleteEmpleado(String id) {
		// TODO Auto-generated method stub
		iEmpleadoDao.deleteById(id);
	}
	

}
