insert into departamentos(id,nombre, presupuesto)values(1,'Dep 1', 1100);
insert into departamentos(id,nombre, presupuesto)values(2,'Dep 2', 2100);

insert into empleados(dni,nombre,apellido,dep_id)values('6332888B','Juanito','Perez',2);
insert into empleados(dni,nombre,apellido,dep_id)values('3666663T','Samuel','Santo',1);
insert into empleados(dni,nombre,apellido,dep_id)values('5999882S','Mohamed','Ali',1);